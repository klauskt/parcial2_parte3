﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parcial2_p3
{
    class Program
    {
        static void Main(string[] args)
        {
            float[] arr = { 9, 8, 7, 6, 5, 4, 3, 2, 1 };
            Insertion.Sort(arr);

            for(int i = 0; i < arr.Length; i++)
            {
                Console.WriteLine(arr[i]);
            }
        }
    }
}
